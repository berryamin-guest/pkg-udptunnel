udptunnel (1.1-5) unstable; urgency=medium

  * DH level updated to 9.
  * Switch from autoconf to autoreconf.
  * debian/control
    - More info in Description.
    - Standards-Version updated to 3.9.7
    - Vcs-* fields to canonical URI.
  * debian/copyright
    - Added Jonathan Lennox to Copyright clause.
    - Added previous Debian Maintainers to Copyright clause.
    - Updated to 1.0 Machine-readable format.
  * debian/dirs
    - Removed, as it is dispensable.
  * debian/rules
    - Added Hardening options.
  * debian/watch
    - Improved URL regexp.
    - Updated to version 4.

 -- Marcos Talau <talau@users.sourceforge.net>  Mon, 04 Apr 2016 19:44:41 -0300

udptunnel (1.1-4) unstable; urgency=low

  * New maintainer (Closes: #590779)
    - Thanks to Chris Lamb
  * debian/control
    - New Vcs-* address
    - Standards-Version updated to 3.9.2
      - No changes need
  * config.* is now updating with debhelper
  * Switch to dpkg-source 3.0 (quilt) format
  * DEP-3 in patches
  * DEP-5 in debian/copyright
  * debian/compat
    - Compatibility level to 8

 -- Marcos Talau <talau@users.sourceforge.net>  Fri, 21 Oct 2011 10:27:03 -0200

udptunnel (1.1-3) unstable; urgency=low

  * Update maintainer email address.
  * Update Git repository locations.
  * Use `dh_prep` over deprecated `dh_clean -k`.
  * Add patch description for 02-strlen-prototype.diff.

 -- Chris Lamb <lamby@debian.org>  Mon, 16 Feb 2009 02:50:52 +0000

udptunnel (1.1-2) unstable; urgency=low

  * New maintainer (Closes: #472603)
  * Acknowledge NMU (Closes: #254834)
  * Rework packaging:
    - Move to debhelper 7.
    - Extract patches and adopt quilt patch system.
    - Move manpage to under debian/.
    - Add Build-Depends on autotools-dev.
    - Move to machine-parsable debian/copyright.
    - Add debian/watch file.
  * Add 02-strlen-prototype.diff to avoid implicit definition of strlen().

 -- Chris Lamb <chris@chris-lamb.co.uk>  Thu, 28 Aug 2008 07:42:58 +0100

udptunnel (1.1-1.1) unstable; urgency=low

  * NMU: Change htons()->htonl() to fix multicast (Closes: #254834).

 -- Paul Sladen <debian@paul.sladen.org>  Sun, 21 May 2006 15:00:02 +0100

udptunnel (1.1-1) unstable; urgency=low

  * Initial Release.

 -- Thomas Scheffczyk <thomas.scheffczyk@verwaltung.uni-mainz.de>  Wed,  2 Apr 2003 15:55:23 +0200

